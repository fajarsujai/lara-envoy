@servers(['web' => 'ubuntu@13.212.164.218'])

@setup
    $app_dir = '/var/www/lara-envoy';
    $release = date('YmdHis');
@endsetup

@story('deploy')
    pull_repository
    run_composer
@endstory

@task('pull_repository')
    echo 'Pull repository'
    cd {{ $app_dir }}
    git pull origin main 
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $app_dir }}
    composer install --prefer-dist --no-scripts -q -o
@endtask

