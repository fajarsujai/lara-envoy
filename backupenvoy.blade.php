@servers(['web' => 'ubuntu@13.212.164.218'])

@setup
    $repository = 'git@gitlab.com:fajarsujai/lara-envoy.git';
    $releases_dir = '/var/www/app/releases';
    $app_dir = '/var/www/lara-envoy';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
    $app_name = 'lara-envoy';
@endsetup

@story('deploy')
    clone_repository
    run_composer
    update_symlinks
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $new_release_dir }} ] || sudo mkdir -p {{ $new_release_dir }}
    git clone --depth 1 {{ $repository }} 
    sudo mv {{ $app_name }} {{ $new_release_dir }}
    sudo chmod 755 {{ $new_release_dir }}
    cd {{ $new_release_dir }}/{{ $app_name }}
    git reset --hard {{ $commit }}
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}/{{ $app_name }}
    composer install --prefer-dist --no-scripts -q -o
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/{{ $app_name }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/{{ $app_name }}/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/{{ $app_name }}/.env

    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }}/{{ $app_name }} {{ $app_dir }}
@endtask